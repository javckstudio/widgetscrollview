-----------------------------------------------------------------------------------------
--
-- main.lua
-- 本範例示範如何使用Widget的ScrollView API
-- Author:Zack Lin
-- Time:2015/4/8
-----------------------------------------------------------------------------------------

--=======================================================================================
--引入各種函式庫
--=======================================================================================
display.setStatusBar( display.HiddenStatusBar )

local widget = require( "widget" )
--=======================================================================================
--宣告各種變數
--=======================================================================================
_SCREEN = {
	WIDTH = display.viewableContentWidth,
	HEIGHT = display.viewableContentHeight
}
_SCREEN.CENTER = {
	X = display.contentCenterX,
	Y = display.contentCenterY
}
local scrollView
local img_bg
local label

local initial
local scrollListener
--=======================================================================================
--宣告與定義main()函式
--=======================================================================================
local main = function (  )
	initial()
end
--=======================================================================================
--定義其他函式
--=======================================================================================
initial = function (  )
	-- 生成ScrollView Widget
	scrollView = widget.newScrollView
	{
	    top = 100,
	    left = 10,
	    width = 600,
	    height = 400,
	    horizontalScrollDisabled = false, --設定水平方向是否要鎖住滑動
	    verticalScrollDisabled = false, --設定垂直方向是否要鎖住滑動
	    isLocked = false, --是否將所有Scroll鎖住
	    isBounceEnabled = true, --是否使用滑動回彈的效果
	    --scrollWidth = 600,
	    --scrollHeight = 800,
	    listener = scrollListener
	}

	-- 生成ScrollView裡頭的元件，最後insert到ScrollView中，如同group一般
	img_bg = display.newImageRect( "bg.jpg", 600, 800 )
	--建議將ScrollView子元件的錨點都設於左上角為宜
	img_bg.anchorX = 0
	img_bg.anchorY = 0
	scrollView:insert( img_bg )

	label = display.newText( "待機中..", 0, 0, system.nativeFont , 40 )
	label.x = _SCREEN.CENTER.X
	label.y = _SCREEN.CENTER.Y + 200
end

-- ScrollView listener
scrollListener = function(event)
	--監控phase屬性
    local phase = event.phase
    if ( phase == "began" ) then label.text = "Scroll view was touched"
    elseif ( phase == "moved" ) then label.text = "Scroll view was moved"
    elseif ( phase == "ended" ) then label.text = "Scroll view was released"
    end

    -- 當Scroll到底的時候，limitReached屬性就會為true
    if ( event.limitReached ) then
        if ( event.direction == "up" ) then label.text = "Reached Bottom limit"
        elseif ( event.direction == "down" ) then label.text = "Reached Top limit"
        elseif ( event.direction == "left" ) then label.text = "Reached Right limit"
        elseif ( event.direction == "right" ) then label.text = "Reached Left limit"
        end
    end

    return true
end
--=======================================================================================
--呼叫主函式
--=======================================================================================
main()


